package main;

import implementation.NoiseAverageGreyScale;
import implementation.NoiseImageColor;
import implementation.NoiseImageGreyScale;
import noise.StaticNoise;
import noise.WorleyNoise;

public class Main {

	public static void main(String[] args) {
		
		StaticNoise staticNoise = new StaticNoise();
		
		WorleyNoise worleyNoise = new WorleyNoise(100,100);
		worleyNoise.initValues();
		worleyNoise.normalizeValues();
		
		NoiseImageGreyScale greyScale = new NoiseImageGreyScale(100,100);
		greyScale.setFileName("worley.png");
		greyScale.implementNoise(worleyNoise);
		greyScale.setFileName("static.png");
		greyScale.implementNoise(staticNoise);
				
		NoiseAverageGreyScale average = new NoiseAverageGreyScale(100,100);
		average.setFileName("average.png");
		average.implementNoise(worleyNoise, staticNoise);
		
		WorleyNoise worleyTwo = new WorleyNoise(100,100);
		worleyTwo.initValues();
		worleyTwo.normalizeValues();
		
		WorleyNoise worleyThree = new WorleyNoise(100,100,30);
		worleyThree.initValues();
		worleyThree.normalizeValues();

		NoiseImageColor color = new NoiseImageColor(100,100);
		color.setFileName("color.png");
		color.implementNoise(worleyNoise, worleyTwo, worleyThree);
	}
}
