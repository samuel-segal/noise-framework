package noise;


/**
 * Returns a psuedorandom value from 0 - 1
 * @author Samuel Segal
 */
public class StaticNoise implements Noise{

	@Override
	public double get(double x, double y) {
		return Math.random();
	}

}