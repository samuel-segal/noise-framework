package noise;

import java.awt.geom.Point2D;
import java.util.Arrays;


/**
 * Basic n = 0 Worley Noise implementation
 * @author Samuel Segal
 */
public class WorleyNoise extends ProperNoise {
	
	Point2D.Double[] points;
	
	public WorleyNoise(int width, int height, int numPoints) {
		super(width,height);
		points = new Point2D.Double[numPoints];
		Arrays.asList(points).replaceAll( (x) -> (
				new Point2D.Double(Math.random()*width,Math.random()*height)
				));
	}
	
	public WorleyNoise(int width, int height) {
		this(width, height,10);
	}

	@Override
	protected double getValue(double x, double y) {
		double minDist = Double.MAX_VALUE;
		for(Point2D.Double p:points) {
			double dist = p.distance(x, y);
			minDist = Math.min(dist, minDist);
		}
		return minDist;
	}

}