package noise;

/**
 * Base noise interface
 * @author Samuel Segal
 */
public interface Noise {

	/**
	 * Returns the value of the noise function at point (x,y)
	 * @param x The x position of the candidate point
	 * @param y The y position of the candidate point
	 * @return The resultant value of the noise function
	 */
	double get(double x, double y);
}
