package noise;


/**
 * A noise function that returns a value between 0 - 1, as normalized by the normalizeValues method.
 * Because of the nature of most noise functions, the length and width are defined on initialization
 * @author Samuel Segal
 */
public abstract class ProperNoise implements Noise {

	private double[][] values;
	private int width,height;
	
	/**
	 * @param width Width of the bounding box
	 * @param height Height of the bounding box
	 */
	public ProperNoise(int width,int height) {
		this.width = width;
		this.height = height;
		this.values = new double[width][height];
	}
	
	
	/**
	 * @param x The x position of the candidate point
	 * @param y The y position of the candidate point
	 * @return The resultant value of the noise function
	 */
	protected abstract double getValue(double x, double y);
	
	
	/**
	 * Sets up the values that the noise function returns
	 */
	public void initValues() {
		for(int x=0;x<values.length;x++) {
			for(int y=0;y<values[x].length;y++) {
				values[x][y] = getValue(x,y);
			}
		}
	}
	
	
	/**
	 * Converts all values initialized previously into a scalar between 0 and 1
	 */
	public void normalizeValues() {
		double min = Double.MAX_VALUE, max = Double.MIN_VALUE;
		for(int x=0;x<values.length;x++) {
			for(int y=0;y<values[x].length;y++) {
				min = Math.min(values[x][y], min);
				max = Math.max(values[x][y], max);
			}
		}
		
		double diff = max-min;
		
		for(int x=0;x<values.length;x++) {
			for(int y=0;y<values[x].length;y++) {
				values[x][y] = (values[x][y]-min)/diff;
			}
		}
	}
	
	
	@Override
	public double get(double x, double y) {
		return values[(int)x][(int)y];
	}

	/**
	 * @return The width of the noise bounding box
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * @return The height of the noise bounding box
	 */
	public int getHeight() {
		return height;
	}
}
