package implementation;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import noise.Noise;

/**
 * A noise implementation that takes 3 noise objects, and uses each for the red green and blue channels of the image
 * @author Samuel Segal
 */
public class NoiseImageColor implements TripleNoiseImplementation {

private int width,height;
	
	private String fileName = "image.png";
	
	public NoiseImageColor(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	
	@Override
	public void implementNoise(Noise noise1, Noise noise2, Noise noise3) {
		BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
		
		for(int x=0;x<width;x++) {
			for(int y=0;y<height;y++) {
				int r = (int)(noise1.get(x, y)*256)&0xFF,
					g = (int)(noise2.get(x, y)*256)&0xFF,
					b = (int)(noise3.get(x, y)*256)&0xFF;
					
				
				image.setRGB(x, y, new Color(r,g,b).hashCode());
			}
		}
		
		try {
			ImageIO.write(image, "png", new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
