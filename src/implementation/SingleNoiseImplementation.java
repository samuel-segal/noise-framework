package implementation;

import noise.Noise;

/**
 * An noise implementation that uses only a single noise object
 * @author Samuel Segal
 */
public interface SingleNoiseImplementation extends NoiseImplementation {

	void implementNoise(Noise noise);
	
}