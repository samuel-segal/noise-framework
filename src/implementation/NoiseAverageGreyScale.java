package implementation;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import noise.Noise;

/**
 * A noise implementation that averages the values of two noise objects, and converts it into a greyscale image
 * @author Samuel Segal
 */
public class NoiseAverageGreyScale implements DoubleNoiseImplementation{

	private int width,height;
	
	private String fileName = "image.png";
	
	public NoiseAverageGreyScale(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	@Override
	public void implementNoise(Noise noise1, Noise noise2) {
		BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);

		for(int x=0;x<width;x++) {
			for(int y=0;y<height;y++) {
				int n = (int)((noise1.get(x, y)+noise2.get(x, y))*127)&0XFF;
				
				image.setRGB(x, y, new Color(n,n,n).hashCode());
			}
		}

		try {
			ImageIO.write(image, "png", new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
