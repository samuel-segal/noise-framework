package implementation;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import noise.Noise;


/**
 * A noise implementation that converts a 0-1 noise scalar into an interpolation between black and white
 * @author Samuel Segal
 */
public class NoiseImageGreyScale implements SingleNoiseImplementation {

	private int width,height;
	
	private String fileName = "image.png";
	
	public NoiseImageGreyScale(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	
	@Override
	public void implementNoise(Noise noise) {
		BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
		
		for(int x=0;x<width;x++) {
			for(int y=0;y<height;y++) {
				int n = (int)(noise.get(x, y)*256)&0XFF;
				
				image.setRGB(x, y, 0x010101*n);
			}
		}
		
		try {
			ImageIO.write(image, "png", new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
