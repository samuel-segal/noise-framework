package implementation;

import noise.Noise;

/**
 * A noise implementation that uses two noise objects
 * @author Samuel Segal
 */
public interface DoubleNoiseImplementation extends NoiseImplementation{
	void implementNoise(Noise noise1, Noise noise2);
}
