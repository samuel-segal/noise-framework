package implementation;

import noise.Noise;

/**
 * A noise implementation that uses three different noise objects
 * @author Samuel Segal
 */
public interface TripleNoiseImplementation extends NoiseImplementation {

	void implementNoise(Noise noise1, Noise noise2, Noise noise3);
	
}
